About
-----
This is a small image for running sqlite from the command line. It uses Alpine Linux.

Usage
----
docker run -it -v ~/data:/data --name sqlite tcgerlach/sqlite

Git Repository
-------------
https://bitbucket.org/tcgerlach/sqlite.git
